package com.company;

import com.company.model.Transaction;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        double tram = 0;
        Scanner sc = new Scanner(System.in);
        Scanner sc1 = new Scanner(System.in);

        System.out.println("Enter the transaction details");
        System.out.println("Enter the account number");
        String acn = sc.nextLine();

        System.out.println("Enter the available amt");
        double available = sc.nextDouble();

        Transaction transaction = new Transaction(acn, available);

        while (true) {
            System.out.println("Enter the transaction amt");
            tram = sc.nextDouble();
            if (transaction.validate(tram)) {
                System.out.println("Do you want to enter more ? Yes/No");
                String moreNeed = sc1.nextLine();
                if (moreNeed.equals("No")) {
                    break;
                }
            } else {
                break;
            }
        }
    }
}

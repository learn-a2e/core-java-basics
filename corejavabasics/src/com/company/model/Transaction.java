package com.company.model;

import com.company.exception.ManualException;

public class Transaction {
    private String accountNumber;
    private Double amount;

    public Transaction() {
    }

    public Transaction(String accountNumber, Double amount) {
        this.accountNumber = accountNumber;
        this.amount = amount;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public boolean validate(Double transactionAmt) {
        double minimalBalance = 500;
        try {
            if ((transactionAmt > amount) || (amount == minimalBalance)) {
                throw new ManualException("Insufficient Balance");
            } else {
                amount = amount - transactionAmt;
                return true;
            }
        } catch (ManualException e) {
            System.out.println("Insufficient Balance");
            return false;
        } finally {
            System.out.println("Your available balance:" + (amount));
        }
    }
}

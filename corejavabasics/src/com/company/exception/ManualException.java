package com.company.exception;

public class ManualException extends Exception {
    public ManualException(String message){
        super(message);
    }
}
